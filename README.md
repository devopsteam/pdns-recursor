# README #

### What is this repository for? ###

* This is a minimalist playbook for installing the PowerDNS recursor (pdns-recusrsor)
* Version 1.0
* This playbook uses the existing ansible role https://github.com/mrlesmithjr/ansible-powerdns-recursor


### How do I get set up? ###

* specify any additional variales in the site.yml file
* create a Project in ansible Tower using this git repo as the source SCM -> sync the project
* create an inventory with the hosts you want the playbook to apply to
* create a job template specifying site.yml as the playbook file
* run the job template and watch it install and configure pdns-recursor on you specified server(s)
